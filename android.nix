{
  lib
, pkgs
, stdenv
, runCommand
, writeTextDir
, buildFHSUserEnv
, fetchRepoProject
, gitRepo
, cacert
, stdenvNoCC
}: {
    repoUrl        ? "git://github.com/LineageOS/android.git"
  , branch         ? "lineage-17.0"
  , model          ? "sagit"
  , name           ? "lineage-sagit"
  , version        ? 16
  , localManifests ? {
    "wireguard.xml" = ''
    <?xml version="1.0" encoding="UTF-8"?>
    <manifest>
      <remote name="zx2c4" fetch="https://git.zx2c4.com/" />
      <project remote="zx2c4" name="android_kernel_wireguard" path="kernel/wireguard" revision="master" sync-s="true" />
    </manifest>
    '';
  }
  , updateImage    ? null
}:
let
  lineagefhs = buildFHSUserEnv {
    name = "lineagefhs";

    targetPkgs = a: with a; [
      androidenv.androidPkgs_9_0.platform-tools

      # 3.1 doesn't work with lineage
      gperf_3_0
      lzop pngcrush rsync schedtool squashfsTools
      gnum4 brotli bc bison ccache git gnupg
      zip ncurses5 pkgconfig wxGTK30 unzip

      (python27.withPackages (a: [a.libxslt]))
    ];

    multiPkgs = a: with a; [
      gcc imagemagick readline zlib SDL openssl libxml2
      curl flex
    ];

    extraOutputsToInstall = [ "dev" ];

  };
  /*
    Creates a directory with text files from attrs.
    defineTextDir {
      "smth.txt" = "mow";
      "smthelse.txt" = "foobar";
    }
  */
  defineTextDir = attrs: with lib;
    map (
      l: writeTextDir l.fst l.snd
    ) (
      zipLists
        (attrNames attrs)
        (attrValues attrs)
    );
  manifests = pkgs.symlinkJoin {
    name = "local-manifests";
    paths = defineTextDir localManifests;
  };
  # I don't like fetchRepo
  lineageSource = stdenvNoCC.mkDerivation {
    name = "source";

    outputHashAlgo = "sha256";
    outputHashMode = "recursive";
    outputHash = "fe8edeeb98cc6d3b93cf2d57000254b84bd9eba34b4df7ce4b87db8b937b7703";

    buildInputs = [ pkgs.gnupg gitRepo pkgs.git cacert ];
    preferLocalBuild = true;
    enableParallelBuilding = true;
    # GIT_SSL_CAINFO = "${cacert}/etc/ssl/certs/ca-bundle.crt";
    buildCommand = ''
    export HOME=$PWD
    mkdir -p $out

    gpg -K
    cd $out

    repo init \
     -u ${repoUrl} \
     -b ${branch} \
     --depth=1

    ln -s ${manifests} .repo/local_manifests

    echo "init"

    repo sync \
     --jobs=$NIX_BUILD_CORES \
     --current-branch \
    | grep From

    rm -rf .repo
    find -type d -name '.git' -prune -exec rm -rf {} +
 '';
  };
in
stdenv.mkDerivation {
  # inherit branch model version;
  # src = lineageSource;
  name = "lineageos-16";
  inherit manifests lineageSource;
  # Don't use ccache if you don't recompile in place .-.
  USE_CCACHE=1;
  buildInputs = [ lineagefhs ];
  # configurePhase = ''
  # source build/envsetup
  # breakfast ${model}
  # croot
  # '';
  builder = "lineagefhs";
}
